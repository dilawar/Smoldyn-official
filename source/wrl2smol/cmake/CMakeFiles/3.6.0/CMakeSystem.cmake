set(CMAKE_HOST_SYSTEM "Darwin-16.7.0")
set(CMAKE_HOST_SYSTEM_NAME "Darwin")
set(CMAKE_HOST_SYSTEM_VERSION "16.7.0")
set(CMAKE_HOST_SYSTEM_PROCESSOR "x86_64")

include("/Users/sandrews/SSA/software/Smoldyn/trunk/Toolchain-mingw32.cmake")

set(CMAKE_SYSTEM "Windows")
set(CMAKE_SYSTEM_NAME "Windows")
set(CMAKE_SYSTEM_VERSION "")
set(CMAKE_SYSTEM_PROCESSOR "")

set(CMAKE_CROSSCOMPILING "TRUE")

set(CMAKE_SYSTEM_LOADED 1)
